# MINI RAG RUST x MISTRAL x QDRANT

> [!IMPORTANT]  
> This repo is strongly based on the Orca RAG example by[@santiagomed](https://github.com/santiagomed)


## Description

Through this repo you will be able to start a small rag based on a pdf of your choice and diriectcly working on CPU. Models used to the embeding is Bert and for chat it is Mistral7B. The VectoR Store use is Qdrant.

> [!WARNING]  
> This RAG works on CPU but can be realy slow ! Do not be surprise if LLM inference take few minutes 😉

### Tree
``` tree
orca-core/ : inference tools package by [@santiagomed](https://github.com/santiagomed)
pdf_fonts-master/ : specific fonts for reading PDF
src/
|---main.rs : RAG code
weights/ :here put your mistral model
Cargo.toml : package specifications
onboarding_lbc.pdf : sample of onboarding confluence
```

## Requirements
[Docker 🐋](https://www.docker.com/products/docker-desktop/)

[Rust 🦀](https://www.rust-lang.org/tools/install)

[Mistral Model (mistral-7b-instruct-v0.1.Q4_K_M.gguf) 🤖](https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.1-GGUF/blob/main/mistral-7b-instruct-v0.1.Q4_K_M.gguf)


## Quickstart

### Qdrant
Start the vector store where your PDF will be store

``` bash
docker pull qdrant/qdrant
docker run -d --name qdrant_test_instance -p 6333:6333 -p 6334:6334 -e QDRANT__SERVICE__GRPC_PORT=6334 qdrant/qdrant
```

### Set Up

#### Model
Put mistral model in `./weights` folder as mentioned in [Tree](#tree)

#### Launch RAG

``` bash
cargo run --release -- --file './rust-book.pdf' --prompt 'What is the difference between an error and panic ?'
```



> [!TIP]Details
> - `--release`  : this option optimised the final build performances of the package but that take more time
> - `--file` : this is the file that will be embeded in the vecto store
> - `--prompt` : this is the first prompt that will start the chat conversation

#### Path fonts

> [!CAUTION]Error
> If you got this :
> ``` bash
>STANDARD_FONTS is not set. Please check https://github.com/pdf-rs/pdf_render/#fonts for instructions.
>note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
>```
> Do following command to export pdf_fonts path like this :

- Windows (cmd) :
``` cmd
set STANDARD_FONTS=%cd%\pdf_fonts-master
```
- PowerShell : 
``` powershell
$env:STANDARD_FONTS = (Get-Location).Path + "\pdf_fonts-master"
```
- Unix :
``` bash
export STANDARD_FONTS=$(pwd)/pdf_fonts-master
```

## Sources
Candle (HuggingFace) :
  - [Git](https://github.com/huggingface/candle) 
  
Orca :
  - [Medium](https://medium.com/@santiagm08/goodbye-python-hello-rust-building-a-rag-cli-application-with-orca-12ff6ce199d3) 
  - [Git](https://github.com/scrippt-tech/orca) 
  